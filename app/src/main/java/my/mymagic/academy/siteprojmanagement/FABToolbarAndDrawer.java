package my.mymagic.academy.siteprojmanagement;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;

/**
 * Created by User on 22-Sep-16.
 */
public class FABToolbarAndDrawer extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ViewGroup mContentViewGroup;
    private NavigationView mNavigationView;
    private Toolbar mToolBar;
    private FloatingActionButton mFloatingActionButton;

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int mMenuItemId = item.getItemId();
        Fragment mMenuSelectedFragment = null;

        //Close the Drawer
        mDrawerLayout.closeDrawers();

        //identify which drawer items was selected
        switch (mMenuItemId) {
            case R.id.main_activity_nav_logout:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;
            case R.id.main_activity_nav_project_summary:
//                Log.d(TAG, "onNavigationItemSelected: Project Summary");
                mMenuSelectedFragment = (Fragment) new RedColorFragment();

                break;
            case R.id.main_activity_nav_read_n_send_message:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;
            case R.id.main_activity_nav_todolist:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();

        }

        //This will close the drawer - to simulate after the selection, the drawer slides away
        if (!mMenuSelectedFragment.equals(null)) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.activity_main_vg_framelayout, mMenuSelectedFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
        }

        return false;
    }




}
