package my.mymagic.academy.siteprojmanagement.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import my.mymagic.academy.siteprojmanagement.R;

/**
 * Created by fahmi.latip on 07/10/2016.
 */

public class UserExecLoginActivity extends AppCompatActivity {

    private EditText mUsername;
    private EditText mPassword;
    private Button mLoginBtn;
    private Button mResetBtn;

    private static String LOGINTAG = "my.mymagic.academy.siteprojmanagement.ui.UserExecLogin";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exec_user_login);

        mUsername = (EditText) findViewById(R.id.user_loginText);
        mPassword = (EditText) findViewById(R.id.user_password);
        mLoginBtn = (Button) findViewById(R.id.exec_user_login_button);
        mResetBtn = (Button) findViewById(R.id.exec_user_reset_button);

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), UserNaviActivity.class);
                intent.putExtra(LOGINTAG, "LOGIN");
                startActivity(intent);
            }
        });
    }
}
