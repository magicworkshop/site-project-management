package my.mymagic.academy.siteprojmanagement.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import my.mymagic.academy.siteprojmanagement.Model.Projects;
import my.mymagic.academy.siteprojmanagement.R;
import my.mymagic.academy.siteprojmanagement.UI.ProjectListingViewHolder;

/**
 * Created by User on 01-Oct-16.
 */

//Every adapter has three primary methods: onCreateViewHolder to inflate the item layout
// and create the holder, onBindViewHolder to set the view attributes based on the data
// and getItemCount to determine the number of items. We need to implement all three to finish the adapter
public class ProjectListAdapter extends RecyclerView.Adapter {

    private List<Projects> mProjectsList = new ArrayList<>();
    private Context mContext;
    public TextView mProjectNameTextView;
    public Button mProjectButtonView;

    //A new variable for RecyclerView Project Listener ***
    private ProjectListingViewHolder.OnSelectProjectListener mProjectListener;

    //ProjectLListner setter
    public void setProjectListener(ProjectListingViewHolder.OnSelectProjectListener projectListener) {
        mProjectListener = projectListener;
    }

    public List<Projects> getProjectsList() {
        return mProjectsList;
    }

    public void setProjectsList(List<Projects> projectsList) {
        mProjectsList.clear();
        mProjectsList.addAll(projectsList);

    }

    //to inflate the item layout and create the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater mProjectSummaryInflater = LayoutInflater.from(context);

//        View mProjectSummaryItemView = mProjectSummaryInflater.inflate(android.R.layout.simple_list_item_1, parent, false); //Simple listing
        View mProjectSummaryItemView = mProjectSummaryInflater.inflate(R.layout.cell_project_summary_item, parent, false);

        // Return a new holder instance + Listener
//        ProjectDetailsNumberViewHolder mProjectSummaryNumberViewHolder = new ProjectDetailsNumberViewHolder(mProjectSummaryItemView);
//        return mProjectSummaryViewHolder;
        return new ProjectListingViewHolder(mProjectSummaryItemView, mProjectListener );
    }

    //to set the view attributes based on the data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//    public void onBindViewHolder(ProjectListAdapter.ProjectDetailsNumberViewHolder holder, int position) {

        ProjectListingViewHolder mProjectListingViewHolder = (ProjectListingViewHolder) holder;
        //Store the list of projects which will be loaded vai the constructor

        Projects mProjects = mProjectsList.get(position);
        mProjectListingViewHolder.setProjects(mProjects, position);
        //To set every row to have the color Pink

    }

    //to determine the number of items
    @Override
    public int getItemCount() {
//        return 50;
        return mProjectsList.size();
    }


    //Crate a constructor that accepts the entire item row
    // and does the view lookups to find each subview
//    public class ProjectDetailsNumberViewHolder extends RecyclerView.ViewHolder {
//
//            public ProjectDetailsNumberViewHolder(View itemView) {
//                super (itemView);
//                mProjectNameTextView = (TextView) itemView.findViewById(R.id.cell_project_summary_viewholder_tv_description);
//                mProjectButtonView = (Button) itemView.findViewById(R.id.cell_project_summary_viewholder_tv_button);
//
//            }
//    }

    //Pass in the Projects array via this Adaptor constructor
    public ProjectListAdapter (Context context, List<Projects> projects) {

        mContext = context;
        mProjectsList = projects;
    }

    public ProjectListAdapter () {

    }


    // Easy access to the context object in the recyclerview
    public Context getContext (){
        return mContext;
    }


}



