package my.mymagic.academy.siteprojmanagement.ui;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import my.mymagic.academy.siteprojmanagement.R;

public class UserNaviActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ViewGroup mContentViewGroup;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_navi);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_navi_main);
        mContentViewGroup = (ViewGroup) findViewById(R.id.nav_vg_content);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);

        // Prepare menu for navigation
        mNavigationView.inflateMenu(R.menu.navi_drawer_menu);
        //mNavigationView.setNavigationItemSelectedListener(this);

    }

    /*public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }*/
}
