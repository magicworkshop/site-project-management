package my.mymagic.academy.siteprojmanagement;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by User on 22-Sep-16.
 */
public class AddTaskActivity extends BaseActivity   {


        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_add_task);

            getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_add_task_vg_key_layout, new AddTaskFragment())
                .commit();
    }

}
