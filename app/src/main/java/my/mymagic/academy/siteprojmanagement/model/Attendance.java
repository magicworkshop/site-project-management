package my.mymagic.academy.siteprojmanagement.model;

import java.util.Date;

/**
 * Created by fahmi.latip on 08/10/2016.
 */

public class Attendance {

    private int id;
    private String userid;
    private Date check_in;
    private Date check_out;
    private Date created_date;
    private String latitude;
    private String longtitude;
    private String storedIdentiti;

    public Attendance(Date check_in, Date check_out, Date created_date, String latitude, String longtitude) {
        this.check_in = check_in;
        this.check_out = check_out;
        this.created_date = created_date;
        this.latitude = latitude;
        this.longtitude = longtitude;
    }

    public Attendance() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Date getCheck_in() {
        return check_in;
    }

    public void setCheck_in(Date check_in) {
        this.check_in = check_in;
    }

    public Date getCheck_out() {
        return check_out;
    }

    public void setCheck_out(Date check_out) {
        this.check_out = check_out;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getStoredIdentiti() {
        return storedIdentiti;
    }
}
