package my.mymagic.academy.siteprojmanagement;

import android.app.Activity;
import android.content.Intent;
import android.nfc.TagLostException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by User on 26-Sep-16.
 */
public class AddTaskFragment extends Fragment {

    private static final String TAG = "AddTaskFragment";
    private static final String TODO_ITEMS = "TODO";

    private Button mAddButton;
    private Button mCancelButton;
    private AppCompatEditText mTodoTaskText;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_task, container, false);

        mAddButton = (Button) rootView.findViewById(R.id.fragment_add_task_add_button);
        mCancelButton = (Button) rootView.findViewById(R.id.fragment_add_task_cancel_button);
        mTodoTaskText = (AppCompatEditText) rootView.findViewById(R.id.fragment_addtask_Text);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int mHello = 1;
        //This is to trigger Intent Action
        actionTaken();

    }

    private  void actionTaken(){
        mAddButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Snackbar.make(getView(), "Add Button Clicked", Snackbar.LENGTH_SHORT).show();


                        if (!mTodoTaskText.getText().toString().isEmpty()) {
                            Intent mReturnIntentResult = new Intent();
                            mReturnIntentResult.putExtra(TODO_ITEMS, mTodoTaskText.getText().toString());
                            //getActivity().setResult(Activity.RESULT_OK); //without result
                            getActivity().setResult(Activity.RESULT_OK, mReturnIntentResult);
                            getActivity().finish();
                        }

                        Snackbar.make(getView(), "Need to have a Todo Task entered", Snackbar.LENGTH_LONG).show();
                    }
                }
        );

        mCancelButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(getView(),"Cancel Button Clicked", Snackbar.LENGTH_SHORT).show();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();

                    }
                }
        );

    }

}



