package my.mymagic.academy.siteprojmanagement.Model;

import android.nfc.TagLostException;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.R.attr.tag;

/**
 * Created by User on 01-Oct-16.
 */
public class Projects {

    private int mProjId;
    private String mProjectName;
    private Calendar mCreatedOn;
    private Calendar mLastUpdateOn;

    public Projects(int projId, String projectName, Calendar createdOn) {
        mProjId = projId;
        mProjectName = projectName;
        mCreatedOn = createdOn;
    }

    public String getProjectName() {
        return mProjectName;
    }

    public Calendar getCreatedOn() {
        return mCreatedOn;
    }

    public int getProjId() {
        return mProjId;
    }



    //    String mProjectDescription = "Empty";
//    int mCreatedByUserId = 0;
//    int mUpdatedByUserId = 0;
//    Date mCreatedDate = (Date) ??
//    Date mUpdatedDate = (Date)



}
