package my.mymagic.academy.siteprojmanagement.UI;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.mymagic.academy.siteprojmanagement.R;

/**
 * Created by User on 06-Oct-16.
 */

public class ProjectDetailsFragment extends Fragment {
    public static final String TAG = "ProjectDetailsFragment";
    TextView mProjectName;
    TextView mCreatedOn;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_project_main_detail_page, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
