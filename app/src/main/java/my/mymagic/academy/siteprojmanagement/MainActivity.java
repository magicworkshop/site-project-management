package my.mymagic.academy.siteprojmanagement;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    //Define the Drawer Layout variables
    private DrawerLayout mDrawerLayout;
    private ViewGroup mContentViewGroup;
    private NavigationView mNavigationView;
    private Toolbar mToolBar;
//    private FloatingActionButton mFloatingActionButton;
//    private ViewGroup mContainerView;
//    private List<String> mTasks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Assign the DrawerLayout variables to the layout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main_vg_drawerlayout);
        mContentViewGroup = (ViewGroup) findViewById(R.id.activity_main_vg_framelayout);
        mNavigationView = (NavigationView) findViewById(R.id.activity_main_vg_navigation_view);
        mToolBar = (Toolbar) findViewById(R.id.activity_vg_main_toolbar);

//        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.activity_main_fab);

        //To display the Toolbar
        setSupportActionBar(mToolBar);

        //Create ActionBar (with Hamburger logo)
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolBar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(drawerToggle); //To informed if the drawer is opened or closed
        drawerToggle.syncState(); //Display the hamburger logo

        //Prepare Drawer layout menu items
        mNavigationView.inflateMenu(R.menu.main_activity_navigation_menu_file);

        //Prepare Drawer listener to listen to the menu selected
        mNavigationView.setNavigationItemSelectedListener(this);


        //Prepare listener for FAB
//        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d(TAG, "onClick: FAB button was clicked ");
////                Snackbar.make(R.layout.activity_main,"FAB Button Clicked", Snackbar.LENGTH_LONG).show();
//                showAddTaskActivity();
//
//            }
//        });

        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_main_vg_framelayout, new WelcomeFragment())
                .commit();
    }

//      FAB add tasks functions
//    public void showAddTaskActivity () {
//        //
//        Intent mLauchAddTaskActivityIntent = new Intent(this, AddTaskActivity.class);
//        // startActivity(mLauchAddTaskActivityIntent); // This is for starting an Activity without result returned
//        startActivityForResult(mLauchAddTaskActivityIntent,ADD_TASK);
//    }

    //Add this to detect the result return by the called Activity
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Log.d(TAG, "We are Entering onActivityResult at MainActivity");
//        //To check is this result is from the Activity Add Task where ADD_TASK is from this.showAddTaskActivity method
//        if (requestCode == ADD_TASK) {
//            String mMessage = "Check Result";
//            switch (resultCode) {
//                case Activity.RESULT_OK:
//                    mMessage = "Clicked OK";
//                    String mTodoItem = data.getStringExtra("todo");
////                    addTaskMethod(mTodoItem);
//                    break;
//                case Activity.RESULT_CANCELED:
//                    mMessage = "Clicked Cancelled";
//                    break;
//                default:
//                    mMessage = "Others";
//            }
//            Log.d(TAG, mMessage);
//        }
//
//    }

//    private void addTaskMethod (String todoItem) {
//        mTasks.add(todoItem);
//
//        TextView taskTextView = (TextView) getLayoutInflater().inflate(R.layout.activity_main, null, false);
//        taskTextView.setText(todoItem);
//        mContainerView.addView(taskTextView);
//
//    }


    // This method to identify which Drawer's item the user tapped on
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int mMenuItemId = item.getItemId();
        Fragment mMenuSelectedFragment = null;

        //Close the Drawer
        mDrawerLayout.closeDrawers();

        //identify which drawer items was selected
        switch (mMenuItemId) {
            case R.id.main_activity_nav_logout:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;
            case R.id.main_activity_nav_project_summary:
//                Log.d(TAG, "onNavigationItemSelected: Project Summary");
                mMenuSelectedFragment = (Fragment) new RedColorFragment();

                break;
            case R.id.main_activity_nav_read_n_send_message:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;
            case R.id.main_activity_nav_todolist:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();

        }

        //This will close the drawer - to simulate after the selection, the drawer slides away
        if (!mMenuSelectedFragment.equals(null)) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.activity_main_vg_framelayout, mMenuSelectedFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
        }

        return false;
    }

}

