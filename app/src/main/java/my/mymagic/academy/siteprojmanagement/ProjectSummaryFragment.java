package my.mymagic.academy.siteprojmanagement;

import android.nfc.TagLostException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Calendar;

import my.mymagic.academy.siteprojmanagement.Adapter.ProjectListAdapter;
import my.mymagic.academy.siteprojmanagement.Model.Projects;
import my.mymagic.academy.siteprojmanagement.R;
import my.mymagic.academy.siteprojmanagement.UI.ProjectDetailsFragment;
import my.mymagic.academy.siteprojmanagement.UI.ProjectListingViewHolder;

/**
 * Created by User on 08-Sep-16.
 */
public class ProjectSummaryFragment extends Fragment implements ProjectListingViewHolder.OnSelectProjectListener, SwipeRefreshLayout.OnRefreshListener {
    String mTxt;

    private static final String TAG = "Project Summary";
    private Button mEndButton;
    private Projects mProjectItem;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout; //To allow recyclerview to refresh when data is added updated or recmove from the recyclerview by swiping
    ProjectListAdapter mProjectListAdapter;

    ArrayList<Projects> mListOfProjects = new ArrayList<Projects>(); //Create an arrayList
    int mProjectsCount = 30; //define the number of projects - auto generated


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_project_summary, container, false);
        mEndButton = (Button) rootView.findViewById(R.id.fragment_project_summary_button);
        //Define recycler View variable
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.project_summary_fragment_vg_recyclerview);
        //Define swipe refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.project_summary_fragment_sr_recyclerview);

        //Just to create a list of projects with Projct + number and current date for this exercise only
        generateListOfProjects (mProjectsCount);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //To setup Recycler Layout manager to Vertical
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false );
        //Or to Grid you do this
//        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),3);
        //Either Grid or Linear, you do need to setLayoutManager below
        mRecyclerView.setLayoutManager(layoutManager);

        //To prepare Recyclerview Adapter
        mProjectListAdapter = new ProjectListAdapter(getContext(),mListOfProjects);
        //Prepare Project Rcycler View List Listener
        mProjectListAdapter.setProjectListener(this); //This will need a method generated within this class call onProjectSelected
        mRecyclerView.setAdapter(mProjectListAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(this); //This will need a new method generated within this class call onRefresh

        mEndButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view) {
                //To end ProjectActivity and get
                getActivity().finish();
            }
        });
    }

    //Temporary generating a list of projects
    public void generateListOfProjects (int numProjects) {
        Calendar mToday = Calendar.getInstance();
        for (int i = 1; i <= numProjects; i++) {
            mListOfProjects.add(new Projects(i, "Project " + i, mToday ));

        }
    }



    public void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume : Showing on screen");
    }

    //    When this fragment about to leave
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "OnPause : leaving the screen");
    }


    @Override
    public void onProjectSelected(Projects projects) {
        Snackbar.make(getView(), projects.getProjectName(), Snackbar.LENGTH_LONG).show();
        ProjectSummaryFragment.this.projectDetails();
    }

    private void projectDetails() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_project_vg_framelayout, new ProjectDetailsFragment())
                .addToBackStack(ProjectDetailsFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    //This is to enable the project list to be refreshed with a swipe after data are added or removed.
    @Override
    public void onRefresh() {
        //End Refreshing animation
        mSwipeRefreshLayout.setRefreshing(false);

        //Rebuild the list of the projects with 3 more projects.. for simulation purposes only
        generateListOfProjects (mProjectsCount+3);

        //Notify recycler view the list has changed
        mProjectListAdapter.notifyDataSetChanged();

    }

}
