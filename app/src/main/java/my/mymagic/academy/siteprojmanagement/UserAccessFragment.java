package my.mymagic.academy.siteprojmanagement;

/**
 * Created by User on 08-Sep-16.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class UserAccessFragment extends Fragment{

    //logt <Tab>
    public static final String TAG = "UserAccessFragment";

    private Button mLoginButton;
    private Button mCreateUsersButton;
    private String mTxt;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_useraccess, container, false);

        mLoginButton = (Button) rootView.findViewById(R.id.fragment_useraccess_login_button);
        mCreateUsersButton = (Button) rootView.findViewById(R.id.fragment_useraccess_createusers_button);
        Log.d(TAG, "onCreateView: ");

        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLoginButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick (View view) {
                mTxt = "Login";
                UserAccessFragment.this.ProjectSummaryFragment();
            }
        });

        mCreateUsersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {

                mTxt = "Create Users";
                UserAccessFragment.this.CreatePlannerUsersFragment();
            }

        });
    }

    public void CreatePlannerUsersFragment() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_framelayout, new CreatePlannerUsersFragment())
                .addToBackStack("PlannerUserAccessFragmentCreate")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    public void ProjectSummaryFragment() {

//        Snackbar.make(getView(),"Project Summary from Activity", Snackbar.LENGTH_SHORT).show();

        //Start a new activity - Project Planning
        Intent mProjectActivity = new Intent(getContext(),ProjectActivity.class);
        startActivity(mProjectActivity);

        //This is used to call another fragment
//        getActivity().getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.activity_main_vg_framelayout, new ProjectSummaryFragment())
//                .addToBackStack("PlannerUserAccessFragmentCreate")
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .commit();
    }


        //    When this fragment is about to appear
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume : Showing on screen");
    }

//    When this fragment about to leave
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "OnPause : leaving the screen");
    }
}
