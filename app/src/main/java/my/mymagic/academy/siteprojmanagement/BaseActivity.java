package my.mymagic.academy.siteprojmanagement;



import android.app.NotificationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.app.Notification;
import android.support.v7.app.NotificationCompat;

/**
 * Created by User on 27-Sep-16.
 */
public class BaseActivity extends AppCompatActivity {

    public void postNotificationMessage (String p_title, String p_subtitle) {
        //Build a Notification small icon , title and detail
        NotificationCompat.Builder mNotificationBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle (p_title)
                .setContentText(p_subtitle);

        Notification mNotification  = mNotificationBuilder.build();

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        int notificationId = (int) (Math.random()*100); //for identification
        mNotificationManager.notify(notificationId, mNotification);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        postNotificationMessage("onCreate", this.getClass().getSimpleName());
    }

    @Override
    protected void onStop() {
        super.onStop();
        postNotificationMessage("onStop", this.getClass().getSimpleName());
    }

    @Override
    protected void onStart() {
        super.onStart();
        postNotificationMessage("onStart", this.getClass().getSimpleName());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        postNotificationMessage("onRestart", this.getClass().getSimpleName());
    }

    @Override
    protected void onPause() {
        super.onPause();
        postNotificationMessage("onPause", this.getClass().getSimpleName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        postNotificationMessage("onResume", this.getClass().getSimpleName());
    }

}


