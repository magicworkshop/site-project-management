package my.mymagic.academy.siteprojmanagement;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.EventLogTags;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by User on 08-Sep-16.
 */
public class CreatePlannerUsersFragment extends Fragment {

    /**
     * Created by User on 08-Sep-16.
     */
        String mTxt;
        private static final String TAG = "CreatePlannerUsers";
        private Button mCreateButton;


        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_signup, container, false);
            mCreateButton = (Button) rootView.findViewById(R.id.fragment_signup_create_button);
            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            mCreateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    CreatePlannerUsersFragment.this.createPlannerAccess();

                }
            } );

        }
    public void createPlannerAccess() {
        //Todo create users access and show a popup message
        getActivity().getSupportFragmentManager()
                .popBackStack("PlannerUserAccessFragmentCreate",1);
    }



    public void onResume() {
            super.onResume();
            Log.d(TAG, "OnResume : Showing on screen");
        }

        //    When this fragment about to leave
        @Override
        public void onPause() {
            super.onPause();
            Log.d(TAG, "OnPause : leaving the screen");
        }

}
