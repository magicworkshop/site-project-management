package my.mymagic.academy.siteprojmanagement;

/**
 * Created by User on 08-Sep-16.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;



import my.mymagic.academy.siteprojmanagement.ui.UserExecLoginActivity;

public class WelcomeFragment extends Fragment {

    private static final String TAG = "WelcomeFragment";
    private static final int ADD_TASK = 1;
    private static final String TODO_ITEMS = "TODO";

    private Button mProjectManagerButton;
    private Button mExecutorButton;
    private String mTxt = "Hello";
    private ViewGroup mContainerView;
    private List<String> mTasks = new ArrayList<>();


    private FloatingActionButton mFloatingActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_welcome_page, container, false);

        mContainerView = (ViewGroup) rootView.findViewById(R.id.fragment_welcome_page_vg_item_list_container);
        mProjectManagerButton = (Button) rootView.findViewById(R.id.fragment_WelcomePage_Planning_button);
        mExecutorButton = (Button) rootView.findViewById(R.id.fragment_WelcomePage_Execution_button);

        mFloatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.welcome_fragement_fab_button);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Prepare listener for FAB
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: FAB button was clicked ");
                Snackbar.make(getView(), "Hello Fragement FAB Button is Clicked", Snackbar.LENGTH_LONG).show();
                showAddTaskActivity();
            }
        });

        mProjectManagerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxt = "Project Manager";
                WelcomeFragment.this.showUserAccess();
            }
        });

        mExecutorButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//Go to Executor Activity Screen
                mTxt = "Executor";

                Intent intent = new Intent(getActivity(), UserExecLoginActivity.class);
                getActivity().startActivity(intent);
            }
        });
    }

    //      FAB add tasks functions
    public void showAddTaskActivity() {
        //
        Intent mLauchAddTaskActivityIntent = new Intent(getContext(), AddTaskActivity.class);
        // startActivity(mLauchAddTaskActivityIntent); // This is for starting an Activity without result returned
        startActivityForResult(mLauchAddTaskActivityIntent, ADD_TASK);
    }

    //Add this to detect the result return by the called Activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "We are Entering onActivityResult at WelcomeFragment");
        //To check is this result is from the Activity Add Task where ADD_TASK is from this.showAddTaskActivity method
        if (requestCode == ADD_TASK) {
            String mMessage = "Check Result";
            switch (resultCode) {
                case Activity.RESULT_OK:
                    mMessage = "Clicked OK";
                    String mTodoItem = data.getStringExtra(TODO_ITEMS);
//                    mTodoItem = "Testing";
                    addTaskMethod(mTodoItem);
                    break;
                case Activity.RESULT_CANCELED:
                    mMessage = "Clicked Cancelled";
                    break;
                default:
                    mMessage = "Others";
            }
            Log.d(TAG, mMessage);
        }
    }

    private void addTaskMethod(String todoItem) {
        mTasks.add(todoItem);

        View taskView = getActivity().getLayoutInflater().inflate(R.layout.cell_todo_item, null, false);
        TextView taskTextView = (TextView) taskView.findViewById(R.id.cell_todo_item_tv_content);
        taskTextView.setText(todoItem);
        mContainerView.addView(taskView);

    }

    public void showUserAccess() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_framelayout, new UserAccessFragment())
                .addToBackStack(UserAccessFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }


}
