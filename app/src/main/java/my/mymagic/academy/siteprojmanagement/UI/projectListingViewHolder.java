package my.mymagic.academy.siteprojmanagement.UI;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import my.mymagic.academy.siteprojmanagement.Model.Projects;
import my.mymagic.academy.siteprojmanagement.R;

/**
 * Created by User on 03-Oct-16.
 */

public class ProjectListingViewHolder extends RecyclerView.ViewHolder{

    private TextView mProjectNameTextView;
    private TextView mCreationDateTextView;
    private Button mProjectButton;

    private Projects mProjects;

    // To create a custom listener when the RecyclerView item is clicked. The listener is at ProjectListingViewHolder
    //+ adding an setOnClickListner at ProjectListingViewHolder and adding a new parameter handler
    //+ adding a Listener at ProjectListAdapter
    public interface OnSelectProjectListener {
        void onProjectSelected (Projects projects);
    }

    public ProjectListingViewHolder(View itemView, final OnSelectProjectListener handler) {
        super (itemView);
        mProjectNameTextView = (TextView) itemView.findViewById(R.id.cell_project_summary_viewholder_tv_description);
        mProjectButton = (Button) itemView.findViewById(R.id.cell_project_summary_viewholder_tv_button);
        mCreationDateTextView = (TextView) itemView.findViewById(R.id.cell_project_summary_viewholder_tv_creation_date);

        itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View itemview){
                handler.onProjectSelected(mProjects);
            }
        });

    }

    public Projects getProjects() {
        return mProjects;
    }

    public void setProjects(Projects projects, int position) {
        mProjects = projects;

        //Calling this method is to refresh the Display when you scroll the RecyclerView
        updateProjectDisplay(position);
    }

    private void updateProjectDisplay (int position) {
        mProjectNameTextView.setText(mProjects.getProjectName());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String createdOn = dateFormat.format (mProjects.getCreatedOn().getTime());
        mCreationDateTextView.setText(createdOn);
        mProjectButton.setText("More...");

        if (position%5 == 0) {
            mProjectNameTextView.setBackgroundColor(R.color.colorAccent);
            mCreationDateTextView.setBackgroundColor(R.color.colorAccent);
        }

    }


}
