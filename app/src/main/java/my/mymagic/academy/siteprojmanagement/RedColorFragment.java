package my.mymagic.academy.siteprojmanagement;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by User on 17-Sep-16.
 */
public class RedColorFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRedFragment = inflater.inflate(R.layout.red_color,container,false);
        return mRedFragment;
    }
}
