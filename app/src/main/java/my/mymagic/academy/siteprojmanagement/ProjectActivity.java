package my.mymagic.academy.siteprojmanagement;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import static android.support.design.widget.NavigationView.*;

/**
 * Created by User on 17-Sep-16.
 */
public class ProjectActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener  {

    private static final String TAG = "ProjectActivity";
    private DrawerLayout mDrawerLayout;
    private ViewGroup mContentViewGroup;
    private NavigationView mNavigationView;
    private Toolbar mToolBar;

//    private FloatingActionButton mFloatingActionButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_project_vg_drawerlayout);
        mContentViewGroup = (ViewGroup) findViewById(R.id.activity_project_vg_framelayout);
        mNavigationView = (NavigationView) findViewById(R.id.activity_project_vg_navigation_view);
        mToolBar = (Toolbar) findViewById(R.id.activity_project_vg_toolbar);

//        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.activity_project_fab);

        //To display the Toolbar
        setSupportActionBar(mToolBar);

        //Create ActionBar (with Hamburger logo)
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolBar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(drawerToggle); //To informed if the drawer is opened or closed
        drawerToggle.syncState(); //Display the hamburger logo

        //Prepare Drawer layout menu items
        mNavigationView.inflateMenu(R.menu.main_activity_navigation_menu_file);

        //Prepare Drawer listener to listen to the menu selected
        mNavigationView.setNavigationItemSelectedListener(this);


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_project_vg_framelayout, new ProjectSummaryFragment())
                .commit();


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int mMenuItemId = item.getItemId();
        Fragment mMenuSelectedFragment = null;

        //Close the Drawer
        mDrawerLayout.closeDrawers();

        //identify which drawer items was selected
        switch (mMenuItemId) {
            case R.id.main_activity_nav_logout:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;

            case R.id.main_activity_nav_project_summary:
//                Log.d(TAG, "onNavigationItemSelected: Project Summary");
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;

            case R.id.main_activity_nav_read_n_send_message:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();
                break;

            case R.id.main_activity_nav_todolist:
                mMenuSelectedFragment = (Fragment) new RedColorFragment();

        }

        //This will close the drawer - to simulate after the selection, the drawer slides away
        if (!mMenuSelectedFragment.equals(null)) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.activity_project_vg_framelayout, mMenuSelectedFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
        }

        return false;
    }
}