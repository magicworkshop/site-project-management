package my.mymagic.academy.siteprojmanagement.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by fahmi.latip on 08/10/2016.
 */

public class Users {

    private int mUserid;
    private String mPicture;
    private int mRole_id;
    private String mName;
    private String mGender;
    private String mEmail;
    private String mPassword;
    private String mHp_no;
    private Calendar mDate_last_login;
    private Calendar mDate_inactive;
    private Calendar mCreated_date;
    private Calendar mLast_updated_date;
    private String mUpdated_by;
    private String mCreated_by;

    public Users(int userid, String picture, int role_id, String name, String gender, String email, String password, String hp_no, Calendar date_last_login, Calendar date_inactive, Calendar created_date, Calendar last_updated_date, String updated_by, String created_by) {
        mUserid = userid;
        mPicture = picture;
        mRole_id = role_id;
        mName = name;
        mGender = gender;
        mEmail = email;
        mPassword = password;
        mHp_no = hp_no;
        mDate_last_login = date_last_login;
        mDate_inactive = date_inactive;
        mCreated_date = created_date;
        mLast_updated_date = last_updated_date;
        mUpdated_by = updated_by;
        mCreated_by = created_by;
    }

    public Users() {
    }

    public int getUserid() {
        return mUserid;
    }

    public void setUserid(int userid) {
        mUserid = userid;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String picture) {
        mPicture = picture;
    }

    public int getRole_id() {
        return mRole_id;
    }

    public void setRole_id(int role_id) {
        mRole_id = role_id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getHp_no() {
        return mHp_no;
    }

    public void setHp_no(String hp_no) {
        mHp_no = hp_no;
    }

    public Calendar getDate_last_login() {
        return mDate_last_login;
    }

    public void setDate_last_login(Calendar date_last_login) {
        mDate_last_login = date_last_login;
    }

    public Calendar getDate_inactive() {
        return mDate_inactive;
    }

    public void setDate_inactive(Calendar date_inactive) {
        mDate_inactive = date_inactive;
    }

    public Calendar getCreated_date() {
        return mCreated_date;
    }

    public void setCreated_date(Calendar created_date) {
        mCreated_date = created_date;
    }

    public Calendar getLast_updated_date() {
        return mLast_updated_date;
    }

    public void setLast_updated_date(Calendar last_updated_date) {
        mLast_updated_date = last_updated_date;
    }

    public String getUpdated_by() {
        return mUpdated_by;
    }

    public void setUpdated_by(String updated_by) {
        mUpdated_by = updated_by;
    }

    public String getCreated_by() {
        return mCreated_by;
    }

    public void setCreated_by(String created_by) {
        mCreated_by = created_by;
    }

    public List<Users> getListUsers () {
        ArrayList<Users>  mUserlist = new ArrayList<Users>();
        Calendar mDatetime = Calendar.getInstance();

        mUserlist.add(new Users(1, "1.jpg", 3, "Andrew Lee", "Male", "andrew.lee@gmail.com", "passwd1", "0123456789", mDatetime, mDatetime, mDatetime, mDatetime, "self", "Admin"));
        mUserlist.add(new Users(2, "2.jpg", 3, "Azman Baharudin", "Male", "azmanlee@gmail.com", "passwd1", "0123456789", mDatetime, mDatetime, mDatetime, mDatetime, "self", "Admin"));
        mUserlist.add(new Users(3, "3.jpg", 3, "Kavita Khapoor", "Female", "kavita@gmail.com", "passwd1", "0123456789", mDatetime, mDatetime, mDatetime, mDatetime, "self", "Admin"));
        mUserlist.add(new Users(4, "4.jpg", 3, "Christopher", "Male", "chris@gmail.com", "passwd1", "0123456789", mDatetime, mDatetime, mDatetime, mDatetime, "self", "Admin"));
        mUserlist.add(new Users(5, "5.jpg", 3, "Sherley West", "Female", "sherley@gmail.com", "passwd1", "0123456789", mDatetime, mDatetime, mDatetime, mDatetime, "self", "Admin"));

        return mUserlist;
    }
}
